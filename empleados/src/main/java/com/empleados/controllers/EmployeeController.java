package com.empleados.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.empleados.model.dto.Employee;
import com.empleados.model.dto.Message;
import com.empleados.model.entities.Empleado;
import com.empleados.services.IEmpleadoService;
import com.empleados.util.MessageFactory;

@RestController
@RequestMapping(path = "/empleados")
public class EmployeeController {

	@Autowired
	@Qualifier("empleadoService")
	private IEmpleadoService empleadoService;

	@Autowired
	private MessageFactory messageFactory;

	@PostMapping("/new")
	public Message crearEmpleado(@RequestBody Employee empleado) {
		if (empleado.isRequiredFieldsNull()) {
			return messageFactory.getErrorCrearEmpleado();
		}
		empleadoService.crear(empleado);
		return messageFactory.getCreacionOk();
	}

	@PutMapping("/update")
	public Message actualizarDatosEmpleado(@RequestBody Employee empleado) {
		if (empleado.isRequiredUpdateFieldsNull()) {
			return messageFactory.getCreacionActualizacionFallida();
		}
		empleadoService.actualizar(empleado);
		return messageFactory.getActualizacionOk();
	}

	@DeleteMapping("/delete/{id}")
	public ResponseEntity<?> eliminarEmpleado(@PathVariable(value = "id") long id) {
		if (id <= 0) {
			return ResponseEntity.ok(messageFactory.getErrorId());
		}
		empleadoService.borrar(id); 	
		return ResponseEntity.ok(messageFactory.getEmpleadoEliminado());
	}

	@GetMapping("/find/{id}")
	public ResponseEntity<?> buscarEmpleado(@PathVariable(value = "id") long id) {
		if (id <= 0) {
			return ResponseEntity.ok(messageFactory.getErrorId());
		}

		return ResponseEntity.ok(empleadoService.consultar(id));
	}

	@PostMapping("/calculosalarial/{id}/{mes}/{anio}")
	public ResponseEntity<?> calcularSalario(@PathVariable(value = "id") long id, @PathVariable(value = "mes") int mes, @PathVariable(value = "anio") int anio) {
		if(id <= 0) {
			return ResponseEntity.ok(messageFactory.getErrorId()) ;
		}
		return ResponseEntity.ok(empleadoService.calcularSalario(id, mes, anio));
	}

}
