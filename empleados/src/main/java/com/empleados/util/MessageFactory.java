package com.empleados.util;

import java.util.Properties;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;

import com.empleados.model.dto.Message;

@Component
public class MessageFactory {

	private static final String CREACION_EMPLEADO_OK = "CREACION_EMPLEADO_OK";
	private static final String CAMPOS_REQUERIDOS_VACIOS = "CAMPOS_REQUERIDOS_VACIOS";
	private static final String ERROR_CREAR_EMPLEADO = "ERROR_CREAR_EMPLEADO";
	private static final String ACTUALIZACION_OK = "ACTUALIZACION_OK";
	private static final String ERROR_ID_INVALIDO = "ERROR_ID_INVALIDO";
	private static final String ERROR_ELIMINAR_EMPLEADO = "ERROR_ELIMINAR_EMPLEADO";
	private static final String EMPLEADO_ELIMINADO_OK = "EMPLEADO_ELIMINADO_OK";

	@Resource(name = "mensajes")
	private Properties mensajes;

	public Message getCreacionOk() {
		return new Message(mensajes.getProperty(CREACION_EMPLEADO_OK));
	}
	

	public Message getCreacionActualizacionFallida() {
		return new Message(mensajes.getProperty(CAMPOS_REQUERIDOS_VACIOS));
	}

	public Message getErrorCrearEmpleado() {
		return new Message(mensajes.getProperty(ERROR_CREAR_EMPLEADO));
	}

	public Message getActualizacionOk() {
		return new Message(mensajes.getProperty(ACTUALIZACION_OK));
	}

	public Message getErrorId() {
		return new Message(mensajes.getProperty(ERROR_ID_INVALIDO));
	}

	public Message getErrorEliminarEmpleado() {
		return new Message(mensajes.getProperty(ERROR_ELIMINAR_EMPLEADO));
	}
	
	public Message getEmpleadoEliminado() {
		return new Message(mensajes.getProperty(EMPLEADO_ELIMINADO_OK));
	}
}
