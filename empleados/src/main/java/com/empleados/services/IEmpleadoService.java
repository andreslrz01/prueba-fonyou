package com.empleados.services;

import org.springframework.stereotype.Service;

import com.empleados.model.dto.Employee;


@Service
public interface IEmpleadoService {
	
	public void crear(Employee empleado);
	public void actualizar(Employee empleado);
	public void borrar(long id);
	public Employee consultar(long id);
	public double calcularSalario(long id, int mes, int anio);

}
