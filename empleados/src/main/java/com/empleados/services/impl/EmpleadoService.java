package com.empleados.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.empleados.model.converter.IConverter;
import com.empleados.model.dto.Employee;
import com.empleados.model.entities.Empleado;
import com.empleados.repositories.EmpleadoRepository;
import com.empleados.services.IEmpleadoService;

@Service
public class EmpleadoService implements IEmpleadoService {
	
	
	@Autowired
	private EmpleadoRepository repository;
	
	@Autowired
	@Qualifier("empleadoConverter")
	private IConverter<Employee, Empleado> converter;
	
	@Override
	public void crear(Employee empleado) {
		Empleado empleadoEntity = converter.toEntity(empleado);
		repository.save(empleadoEntity);	
		repository.flush();
	}

	@Override
	public void actualizar(Employee empleado) {
		if(consultar(empleado.getId()) != null) {
			Empleado empleadoEntity = converter.toEntity(empleado);
			repository.save(empleadoEntity);
			repository.flush();
		}
	}

	@Override
	public void borrar(long id) {
		Empleado empleado = repository.buscarxId(id);
		if(empleado != null) {
			repository.delete(empleado);
		}
	}

	@Override
	public Employee consultar(long id) {
		Empleado empleado = repository.buscarxId(id);
		return converter.toDTO(empleado);
	}

	@Override
	public double calcularSalario(long id, int mes, int anio) {
		Empleado empleado = repository.buscarxId(id);		
		return empleado.calcularSalario(mes, anio);
	}

	
}
