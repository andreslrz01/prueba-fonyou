package com.empleados.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.empleados.model.entities.Empleado;

@Repository
public interface EmpleadoRepository extends JpaRepository<Empleado, Long> {
	
	@Query("SELECT u FROM Empleado u WHERE id = :id") 
	public abstract Empleado buscarxId(@Param("id") Long id); 

}
