package com.empleados.model.dto;

import java.time.LocalDate;

import com.empleados.util.LocalDateDeserializer;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

public class Employee {

	private long id;
	private String nombres;
	private String apellidos;
	@JsonFormat(pattern="yyy-MM-dd")
	@JsonDeserialize(using = LocalDateDeserializer.class)
	private LocalDate fechaIngreso;
	private double salario;
	@JsonFormat(pattern="yyy-MM-dd")
	@JsonDeserialize(using = LocalDateDeserializer.class)
	private LocalDate fechaEgreso;

	public Employee() {
		super();
	}

	public Employee(long id, String nombres, String apellidos, LocalDate fechaIngreso, double salario,
			LocalDate fechaEgreso) {
		super();
		this.id = id;
		this.nombres = nombres;
		this.apellidos = apellidos;
		this.fechaIngreso = fechaIngreso;
		this.salario = salario;
		this.fechaEgreso = fechaEgreso;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNombres() {
		return nombres;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public LocalDate getFechaIngreso() {
		return fechaIngreso;
	}

	public void setFechaIngreso(LocalDate fechaIngreso) {
		this.fechaIngreso = fechaIngreso;
	}

	public double getSalario() {
		return salario;
	}

	public void setSalario(double salario) {
		this.salario = salario;
	}

	public LocalDate getFechaEgreso() {
		return fechaEgreso;
	}

	public void setFechaEgreso(LocalDate fechaEgreso) {
		this.fechaEgreso = fechaEgreso;
	}
	
	public boolean isRequiredFieldsNull() {
		return nombres == null || apellidos == null || fechaIngreso == null || salario <= 0;
	}
	
	public boolean isRequiredUpdateFieldsNull() {
		return nombres == null || apellidos == null || fechaIngreso == null || salario <= 0 || id <= 0;
	}

}
