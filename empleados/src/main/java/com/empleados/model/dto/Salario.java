package com.empleados.model.dto;

public class Salario {
	private long id;
	private double salario;
	
	public Salario() {
		super();
	}

	public Salario(long id, double salario) {
		super();
		this.id = id;
		this.salario = salario;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public double getSalario() {
		return salario;
	}

	public void setSalario(double salario) {
		this.salario = salario;
	}

}
