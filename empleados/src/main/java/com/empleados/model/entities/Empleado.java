package com.empleados.model.entities;

import java.time.Duration;
import java.time.LocalDate;

import javax.annotation.Generated;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "TB_EMPLEADOS")
public class Empleado {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
	
	@Column(length = 50, nullable = false)
	private String nombres;
	
	@Column(length = 50, nullable = false)
	private String apellidos;
	
	@Column(name = "fecha_ingreso", columnDefinition = "DATE", nullable = false)
	private LocalDate fechaIngreso;
	
	@Column(nullable = false)
	private double salario;
	
	@Column(name = "fecha_retiro", columnDefinition = "DATE", nullable = true)
	private LocalDate fechaEgreso;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNombres() {
		return nombres;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public LocalDate getFechaIngreso() {
		return fechaIngreso;
	}

	public void setFechaIngreso(LocalDate fechaIngreso) {
		this.fechaIngreso = fechaIngreso;
	}

	public double getSalario() {
		return salario;
	}

	public void setSalario(double salario) {
		this.salario = salario;
	}

	public LocalDate getFechaEgreso() {
		return fechaEgreso;
	}

	public void setFechaEgreso(LocalDate fechaEgreso) {
		this.fechaEgreso = fechaEgreso;
	}
	
	public double calcularSalario(int mes, int anio) {
		LocalDate fechaConsulta = LocalDate.of(anio, mes, 1);
		int dias = 0;
		if(isMismoMesAnio(fechaConsulta, fechaIngreso) && fechaConsulta.isAfter(fechaIngreso)) {
			if(fechaConsulta.isBefore(fechaIngreso)) {
				fechaConsulta = LocalDate.of(anio, mes, 30);
			}
			dias = (int) Duration.between(fechaIngreso.atStartOfDay(), fechaConsulta.atStartOfDay()).toDays();
		} else if(isMismoMesAnio(fechaConsulta, fechaEgreso) && fechaConsulta.isAfter(fechaEgreso)) {
			if(fechaConsulta.isAfter(fechaEgreso)) {
				fechaConsulta = LocalDate.of(anio, mes, 1);
			}
			dias = (int) Duration.between(fechaConsulta.atStartOfDay(), fechaEgreso.atStartOfDay()).toDays();
		} else if(fechaConsulta.isAfter(fechaIngreso) && fechaConsulta.isBefore(fechaEgreso)) {
			dias = 30;
		}
		return (salario/30)*dias;
	}
	
	private boolean isMismoMesAnio(LocalDate fechaConsulta, LocalDate fecha) {
		return fechaConsulta.getMonthValue() == fecha.getMonthValue() && fechaConsulta.getYear() == fecha.getYear();
	}
	
	

}
