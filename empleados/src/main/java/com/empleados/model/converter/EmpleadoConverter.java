package com.empleados.model.converter;

import org.springframework.stereotype.Component;

import com.empleados.model.dto.Employee;
import com.empleados.model.entities.Empleado;

@Component
public class EmpleadoConverter implements IConverter<Employee, Empleado> {

	@Override
	public Employee toDTO(Empleado t) {
		Employee empleado = new Employee();
		empleado.setId(t.getId());
		empleado.setNombres(t.getNombres());
		empleado.setApellidos(t.getApellidos());
		empleado.setFechaIngreso(t.getFechaIngreso());
		empleado.setSalario(t.getSalario());
		empleado.setFechaEgreso(t.getFechaEgreso());
		return empleado;
	}

	@Override
	public Empleado toEntity(Employee t) {
		Empleado empleado = new Empleado();
		empleado.setId(t.getId());
		empleado.setNombres(t.getNombres());
		empleado.setApellidos(t.getApellidos());
		empleado.setFechaIngreso(t.getFechaIngreso());
		empleado.setSalario(t.getSalario());
		empleado.setFechaEgreso(t.getFechaEgreso());
		return empleado;
	}

	

}
