package com.empleados.model.converter;

import org.springframework.stereotype.Component;

@Component
public interface IConverter<T, E> {
	public T toDTO(E e);
	public E toEntity(T t);
}
